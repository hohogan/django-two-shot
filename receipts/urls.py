from django.urls import path
from receipts.views import (
    create_receipt,
    receipts_list,
    account_list,
    create_account,
    category_list,
    create_category,
)


urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
]
