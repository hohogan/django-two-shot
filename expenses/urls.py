from django.contrib import admin
from django.urls import path, include
from receipts.views import redirect_view


urlpatterns = [
    path("", redirect_view),
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),
]
